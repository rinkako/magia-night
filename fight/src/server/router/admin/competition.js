import express from 'express';
import Competition from '/model/competition';
import wrap from 'express-async-wrap';
import {requireAdmin} from '/utils';

const router = express.Router();

router.get('/now', requireAdmin, wrap(async (req, res) => {
  const lastCompetition = await Competition.find();
  res.send(lastCompetition.toObject());
}));

router.post('/active', requireAdmin, wrap(async (req, res) => {
  const act = await Competition.findOne({active: true});
  if (act) {
    act.active = false;
    await act.save();
  }
  const target = await Competition.findOne({'_id': req.body.id});
  if (target) {
    target.active = true;
    await target.save();
  } else {
    return res.status(400).send(`Competition Not Found?`);
  }
  res.send({
    status: 'success'
  });
}));

router.post('/add', requireAdmin, wrap(async (req, res) => {
  const act = await Competition.findOne({active: true});
  if (act) {
    act.active = false;
    await act.save();
  }
  const competition = new Competition();
  competition.active = true;
  competition.desc = req.body.desc;
  await competition.save();
  res.send({
    status: 'success',
    id: competition._id
  });
}));

router.post('/change', requireAdmin, wrap(async (req, res) => {
  const target = await Competition.findOne({'_id': req.body.id});
  if (target) {
    target.desc = req.body.desc;
    await target.save();
  } else {
    return res.status(400).send(`Competition Not Found?`);
  }
  res.send({
    status: 'success'
  });
}));

export default router;
