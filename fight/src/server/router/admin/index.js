import express from 'express';
import {requireAdmin} from '/utils';
import Competition from './competition';
import Match from './match';

const router = express.Router();

router.use('/', requireAdmin);
router.use('/competition', Competition);
router.use('/match', Match);

export default router;
