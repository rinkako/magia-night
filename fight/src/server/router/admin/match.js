import express from 'express';
import wrap from 'express-async-wrap';
import {requireAdmin} from '/utils';
import Match from '/model/match';
import User from '/model/user';
import Competition from '/model/competition';
import randomString from 'randomstring';
const router = express.Router();

router.post('/new', requireAdmin, wrap(async (req, res) => {
  const act = await Competition.findOne({active: true});
  if (!act) {
    return res.status(401).send(`There is no active competition!`);
  }
  const attacker = User.findOne({username: req.body.attacker});
  const defender = User.findOne({username: req.body.defender});
  if (!attacker) {
    return res.status(401).send(`Attacker Not Found!`);
  }
  if (!defender) {
    return res.status(401).send(`Defender Not Found!`);
  }
  let key = randomString.generate(10);
  while (await Match.findOne({key: key})) {
    key = randomString.generate(10);
  }
  const match = new Match();
  match.applicant = req.user._id;
  match.attacker = attacker._id;
  match.defender = defender._id;
  match.competition = act;
  match.key = key;
  await match.save();
  res.status(401).send({
    status: 'success',
    key: key
  });
}));

router.get('/player', requireAdmin, wrap(async (req, res) => {
  const users = User.find(
    {
      $match: {
        'roles': 'player'
      }
    });
  res.send(users.toObject());
}));

export default router;
