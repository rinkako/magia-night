import express from 'express';
// import Competition from '/model/competition';
import wrap from 'express-async-wrap';
import {requireKey} from '/utils';
// import randomString from 'randomstring';

const router = express.Router();

router.post('/now', requireKey, wrap(async (req, res) => {
  res.send({
    status: req.match.status,
    desc: req.match.competition.desc,
    // magic: req.match.magic,
    attacker: req.match.attacker,
    defender: req.match.defender,
    isAttacker: req.user._id === req.match.attacker._id
  });
}));

const allowTarget = ['create', 'ready', 'start', 'finish'];

router.post('/change', requireKey, wrap(async (req, res) => {
  if (!req.body.target) return res.status(400).send(`What are you doing?`);
  if (req.user._id !== req.match.attacker._id) return res.status(403).send(`Forbidden!`);
  if (req.body.target !== req.match.status) {
    if (!allowTarget.includes(req.match.status)) return res.status(500).send(`Wrong status?!`);
    if (!allowTarget.includes(req.body.target)) return res.status(400).send(`What are you doing?`);
    const first = allowTarget.findIndex(x => x === req.match.status);
    const second = allowTarget.findIndex(x => x === req.body.target);
    if (first + 1 !== second) return res.status(400).send(`Wrong step?`);
    const match = req.match;
    if (req.body.target === 'ready') {
      // match.competition = await Competition.findOne({}, {}, { sort: { 'created_at': -1 } }, (err, post) => {
      //   console.log(err);
      //   console.log(post);
      // });
      // const magic = randomString.generate(10);
      // req.match.magic = magic;
    }
    match.status = req.body.target;
    match.timestamps.push({
      status: req.body.target
    });
    await match.save();
    res.send({
      status: 'success'
    });
  } else {
    return res.status(400).send(`Nothing happen!`);
  }
}));

router.post('/points', requireKey, wrap(async (req, res) => {
  if (!req.body.points) return res.status(400).send(`What are you doing?`);
  if (req.user._id !== req.match.applicant._id) return res.status(403).send(`Forbidden!`);
  if (req.match.status !== 'finish') return res.status(403).send(`Not Finished!`);
  const match = req.match;
  match.points = req.body.points;
  match.status = 'scored';
  match.timestamps.push({
    status: 'scored'
  });
  await match.save();
  res.send({
    status: 'success'
  });
}));

export default router;
