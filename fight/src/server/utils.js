import Match from '/model/match';
import wrap from 'express-async-wrap';

export const requireKey = wrap(async (req, res, next) => {
  if (!req.body.key) return res.status(401).send('Please use the key!');
  // eslint-disable-next-line require-atomic-updates
  const match = await Match.findOne({key: req.body.key}).populate('applicant').populate('attacker', 'meta').populate('defender', 'meta').populate('competition', 'desc');
  if(!match) return res.status(401).send('Match not found!');
  // eslint-disable-next-line require-atomic-updates
  req.match = match;
  if (!req.user || req.user._id !== match.applicant._id) {
    // eslint-disable-next-line require-atomic-updates
    req.user = match.attacker;
  }
  if (!req.user) return res.status(403).send('User not found!');
  next();
});

export const requireLogin = (req, res, next) => {
  if (!req.user) return res.status(401).send(`You are not logged in`);
  next();
};

export const requireAdmin = (req, res, next) => {
  if (!req.user) return res.status(401).send(`You are not logged in`);
  if (!req.user.isAdmin()) return res.status(401).send(`You are not admin`);
  next();
};
