import Admin from './admin';
import match from './match';
import competition from './competition';

export default {
  index: Admin,
  match,
  competition
};
