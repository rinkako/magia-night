import Vue from 'vue';
import html from './match.pug';
import toastr from 'toastr';
import store, {getUser} from 'js/store';

export default Vue.extend({
  data () {
    return {
      players: null,
      result: null,
      attacker: null,
      defender: null
    };
  },
  template: html,
  ready () {
    this.timer = setTimeout(this.updateData, 0);
  },
  beforeDestroy () {
    clearTimeout(this.timer);
    this.timer = null;
  },
  methods: {
    async updateData () {
      clearTimeout(this.timer);
      try {
        const data = (await this.$http.get(`/admin/match/player`));
        console.log(data);
        this.players = data;
      } catch (e) {}
      if (!_.isNil(this.timer)) { this.timer = setTimeout(this.updateData, 10000); }
    }
  },
  async clickSubmit () {
    const attacker = this.attacker;
    const defender = this.defender;
    $('#attacker').dropdown('clear');
    $('#defender').dropdown('clear');
    try {
      const data2 = (await this.$http.post(`/admin/match/new`, {
        attacker: attacker,
        defender: defender
      }));
      if (data2.status !== 'success') {
        throw Error('Failed!');
      }
      this.result = data2.key;
    } catch (e) {
      if ('body' in e) { toastr.error(e.body); } else console.log(e);
    }
  },
  filters: {
  },
  store,
  vuex: {
    getters: {
      user: getUser
    }
  }
});
