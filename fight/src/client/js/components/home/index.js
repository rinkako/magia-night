import Vue from 'vue';
import html from './index.pug';
import store, {userLogin, getUser} from 'js/store';
import toastr from 'toastr';
import _ from 'lodash';
import './index.css';

export default Vue.extend({
  data () {
    return {
      match: {}
    };
  },
  template: html,
  ready () {
  },
  store,
  vuex: {
    actions: {
      userLogin
    },
    getters: {
      user: getUser
    }
  },
  filters: {
  },
  methods: {
  },
  watch: {
  }
});
