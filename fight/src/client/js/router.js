import Vue from 'vue';
import VueRouter from 'vue-router';
import Admin from './components/admin';
import Home from './components/home';
import Match from './components/match';
import Profile from './components/profile';

Vue.use(VueRouter);

const router = new VueRouter({
  linkActiveClass: 'active'
});

router.map({
  '/': {
    name: 'home',
    component: Home
  },
  '/match/:key': {
    name: 'match',
    component: Match
  },
  '/admin': {
    name: 'admin',
    component: Admin.index,
    subRoutes: {
      '/match': {
        name: 'admin.match',
        component: Admin.match
      },
      '/competition': {
        name: 'admin.competition',
        component: Admin.competition
      }
    }
  },
  '/profile': {
    name: 'profile',
    component: Profile
  }
});

router.redirect({
  '/admin': '/admin/match'
});

export default router;
