# Installation
```
# install nvm
# https://github.com/creationix/nvm

# install node
nvm install v10.10.0

# install mongodb
sudo apt install mongodb

# install gulp and forever
npm install -g gulp forever

# Install package, it would take a while
npm install

# Init
gulp init
# Semantic auto install is bugged
# So choose extend my settings > automatic manually when prompted

# Change src/server/config.js
# example: config.example.js

# Build
gulp build

# Unzip fonts.tar.gz in dist/static
tar xvf fonts.tar.gz -C dist/static/

# Run server
./start.sh

```
